import angular from 'angular';
import _ from 'lodash';
import {models} from '@kentcdodds/send-money-common';

const mockTransaction = models.transaction.mock;

export default ngModule => {
  describe('sm-send-money-form', () => {
    beforeEach(window.module(ngModule.name));

    let $compile, scope, el, node;
    const basicTemplate = `
      <sm-send-money-form
        on-submit="onSubmit(transaction)"
        transaction="transaction"
      >
      </sm-send-money-form>
    `;

    beforeEach(inject((_$compile_, $rootScope) => {
      $compile = _$compile_;
      scope = $rootScope.$new();
      scope.onSubmit = sinon.spy();
      scope.transaction = mockTransaction();
    }));

    it(`should call addTransaction when the data is filled in and the form is submitted`, () => {
      compileAndDigest();
      const form = node.querySelector('form');
      angular.element(form).triggerHandler('submit');
      expect(scope.onSubmit).to.have.been.calledOnce;
      expect(scope.onSubmit).to.have.been.calledWith(scope.transaction);
    });

    function compileAndDigest(template = basicTemplate, extraProps = {}) {
      _.assign(scope, extraProps);
      el = $compile(template)(scope);
      node = el[0];
      scope.$digest();
    }
  });
};

