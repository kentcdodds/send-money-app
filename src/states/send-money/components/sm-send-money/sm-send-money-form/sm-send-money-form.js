import smDirective from 'smDirective';
import styles from './sm-send-money-form.css';

export default ngModule => {
  ngModule.directive('smSendMoneyForm', smSendMoneyForm);

  /* istanbul ignore next */
  if (process.env.NODE_ENV === 'test') {
    require('./sm-send-money-form.test')(ngModule);
  }

  function smSendMoneyForm() {
    return smDirective({
      restrict: 'E',
      template: `
        <div class="sm-send-money-form">
          <form
            name="vm.form"
            novalidate
            ng-submit="vm.onSubmit(vm.transaction)"
          >
            <formly-form
              form="vm.form"
              model="vm.transaction"
              fields="vm.fields"
              options="vm.formOptions"
              class="${styles.form}"
            >
            </formly-form>
            <sm-next-previous
              class="${styles.nextPrevButtons}"
              state="vm.formOptions.formState"
              stage-valid="vm.form.$valid"
              max="vm.fields.length - 1"
            >
            </sm-next-previous>
            <div
              class="${styles.submitButtonContainer}"
            >
              <button
                ng-if="vm.formOptions.formState.stage === vm.fields.length - 1 && vm.form.$valid"
                class="btn"
                type="submit"
              >
                Submit
              </button>
              <button
                ng-if="vm.form.$dirty"
                class="btn btn-secondary"
                type="button"
                ng-click="vm.resetForm()"
              >
                Reset
              </button>
            </div>
          </form>
        </div>
      `,
      scope: {
        transaction: '=',
        onSubmit: '&'
      },
      scopeTypes,
      controllerAs: 'vm',
      bindToController: true,
      controller: SmSendMoneyFormCtrl
    });

    // @ngInject
    function SmSendMoneyFormCtrl(smFields) {
      const vm = this;
      vm.resetForm = resetForm;

      vm.formOptions = {formState: {stage: 0}};
      vm.fields = getFields();

      function resetForm() {
        vm.formOptions.resetModel();
        vm.formOptions.formState.stage = 0;
      }

      function getFields() {
        const hideExpression = 'formState.stage !== index';
        const animationClasses = 'animated fadeInUp';
        const className = `${animationClasses} +display-block`;
        const focus = true;
        const required = true;
        return [
          {
            type: 'input',
            key: 'email',
            templateOptions: {
              type: 'email',
              label: 'Who is this for?',
              required,
              placeholder: 'monica.mcgrew@example.com',
              focus,
            },
            className,
            hideExpression,
          },
          smFields.amountField(focus, required, {
            hideExpression,
            className,
          }),
          {
            type: 'textarea',
            key: 'message',
            templateOptions: {
              label: 'Want to make any notes about this?',
              placeholder: `Breakfast at Tiffany's`,
              focus,
            },
            className,
            hideExpression,
          },
          {
            type: 'button-group',
            key: 'purpose',
            templateOptions: {
              label: 'What is this for?',
              options: [
                {label: 'Friends or family', value: 'FAMILY_OR_FRIEND'},
                {label: 'Goods or services', value: 'GOODS_OR_SERVICES'},
              ],
              focus,
              descriptionClassName: styles.purposeDescription,
            },
            expressionProperties: {
              'templateOptions.description'($viewValue) {
                if ($viewValue === 'FAMILY_OR_FRIEND') {
                  return `
                    Free for both of you when sent domestically in USD,
                    and funded with your PayPal balance or bank account.
                  `;
                } else if ($viewValue === 'GOODS_OR_SERVICES') {
                  return `
                    Free for you. The seller pays a small fee to process the payment.
                  `;
                }
              }
            },
            className: `${animationClasses} +display-flex +flex-column +flex-items-center`,
            hideExpression,
          }
        ];
      }
    }

    /* istanbul ignore next */
    function scopeTypes(check) {
      return {
        // normally with more time, I'd
        // define the shape or say this
        // needs to be an instance of a
        // transaction...
        transaction: check.object.optional
      };
    }
  }
};

