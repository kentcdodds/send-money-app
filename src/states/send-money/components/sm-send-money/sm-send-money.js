import styles from './sm-send-money.css';
import smDirective from 'smDirective';
import smSendMoneyFormFactory from './sm-send-money-form';
import smSendMoneyFormProgressFactory from './sm-send-money-form-progress';


export default ngModule => {
  ngModule.directive('smSendMoney', smSendMoney);
  smSendMoneyFormFactory(ngModule);
  smSendMoneyFormProgressFactory(ngModule);

  /* istanbul ignore next */
  if (process.env.NODE_ENV === 'test') {
    require('./sm-send-money.test')(ngModule);
  }

  function smSendMoney() {
    return smDirective({
      restrict: 'E',
      template: `
        <div class="sm-send-money ${styles.root} +text-align-center">
          <div az-promise-show="vm.submitting">
            <div class="${styles.overlay}"></div>
            <div class="${styles.spinnerContainer}">
              <sm-spinner class="${styles.spinner}" text="Sending money...">
              </sm-spinner>
            </div>
          </div>
          <div class="+display-relative ${styles.formContainer}">
            <sm-page-title title="Send Money">
            </sm-page-title>
            <sm-send-money-form-progress
              class="${styles.progress} +transition-medium "
              ng-class="{'+opacity0': !vm.transaction.email, '+opacity1': vm.transaction.email}"
              transaction="vm.transaction"
            >
            </sm-send-money-form-progress>
            <sm-send-money-form
              transaction="vm.transaction"
              on-submit="vm.onSubmit(vm.transaction)"
            >
            </sm-send-money-form>
          </div>
        </div>
      `,
      scope: {},
      controllerAs: 'vm',
      bindToController: true,
      controller: SmSendMoney,
    });

    // @ngInject
    function SmSendMoney(sendMoneyAPI, smUtils, lastTransaction, $state) {
      const vm = this;
      vm.transaction = {}; // <-- this is where you might pre-fill data
      vm.onSubmit = onSubmit;

      function onSubmit(transaction) {
        const promise = sendMoneyAPI
          .addTransaction(transaction)
          .then(result => lastTransaction.transaction = result);
        const time = (Math.random() * 2000) + 3000;
        const minTimePromise = smUtils.promiseMinTime(promise, time);
        vm.submitting = minTimePromise.then(() => $state.go('home'));
        return vm.submitting;
      }
    }
  }
};

