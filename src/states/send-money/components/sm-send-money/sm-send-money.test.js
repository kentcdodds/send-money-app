import _ from 'lodash';

export default ngModule => {
  describe('sm-send-money', () => {
    beforeEach(window.module(ngModule.name));

    let $compile, scope, el, node, isolateScope, vm, sendMoneyAPI;
    const basicTemplate = `<sm-send-money></sm-send-money>`;

    beforeEach(inject((_$compile_, _sendMoneyAPI_, $rootScope) => {
      $compile = _$compile_;
      sendMoneyAPI = _sendMoneyAPI_;
      sendMoneyAPI.addTransaction = sinon.spy(() => ({then: sinon.spy()}));
      scope = $rootScope.$new();
    }));

    it(`should call addTransaction when the data is filled in and the form is submitted`, () => {
      compileAndDigest();
      const smSendMoneyForm = node.getElementsByTagName('sm-send-money-form')[0];
      const submitExpression = smSendMoneyForm.getAttribute('on-submit');
      isolateScope.$eval(submitExpression);
      expect(sendMoneyAPI.addTransaction).to.have.been.calledWith(vm.transaction);
    });

    function compileAndDigest(template = basicTemplate, extraProps = {}) {
      _.assign(scope, extraProps);
      el = $compile(template)(scope);
      node = el[0];
      scope.$digest();
      isolateScope = el.isolateScope();
      vm = isolateScope.vm;
    }
  });
};

