import smDirective from 'smDirective';
export default ngModule => {
  ngModule.directive('smSendMoneyFormProgress', smSendMoneyFormProgress);

  /* istanbul ignore next */
  if (process.env.NODE_ENV === 'test') {
    require('./sm-send-money-form-progress.test')(ngModule);
  }

  function smSendMoneyFormProgress() {
    return smDirective({
      restrict: 'E',
      // thinking about this now, this should probably be
      // refactored into a simple function rather than
      // putting all the logic into this strange DSL-enfused
      // template. But I don't have time to refactor, and this
      // is well tested, so there you go :-)
      template: `
        <div class="sm-send-money-form-progress">
          <span ng-if="vm.transaction.email">
            Sending
            <strong>{{
              (vm.transaction.amount | smCurrency:vm.transaction.currencyType) ||
              'money'
            }}</strong>
            to
            <strong>{{vm.transaction.email}}</strong>{{vm.purposeDisplay[vm.transaction.purpose]}}
          </span>
        </div>
      `,
      scope: {
        transaction: '='
      },
      scopeTypes,
      controllerAs: 'vm',
      bindToController: true,
      controller: SmSendMoneyFormProgress
    });

    // @ngInject
    function SmSendMoneyFormProgress() {
      const vm = this;

      vm.purposeDisplay = {
        GOODS_OR_SERVICES: ' for Goods or Services',
        FAMILY_OR_FRIEND: ', a family member or a friend'
      };
    }

    /* istanbul ignore next */
    function scopeTypes(check) {
      return {
        transaction: check.object.optional
      };
    }
  }
};

