import _ from 'lodash';
import {models} from '@kentcdodds/send-money-common';

const mockTransaction = models.transaction.mock;
export default ngModule => {
  describe('sm-send-money-form-progress', () => {
    beforeEach(window.module(ngModule.name));

    let $compile, scope, el, node, text, currency;
    const basicTemplate = `
      <sm-send-money-form-progress
        transaction="transaction"
      >
      </sm-send-money-form-progress>
    `;
    const compileBasic = compileAndDigest.bind(null, basicTemplate);


    beforeEach(inject((_$compile_, $rootScope, $filter) => {
      $compile = _$compile_;
      scope = $rootScope.$new();
      scope.transaction = mockTransaction();
      currency = $filter('currency');
    }));

    it(`nothing`, () => {
      compileBasic({transaction: {}});
      expect(text).to.be.empty;
    });

    it(`email only`, () => {
      const {email} = scope.transaction;
      compileBasic({transaction: {email}});
      expect(text).to.equal(`Sending money to ${email}`);
    });

    it(`email and currency`, () => {
      const {email, currencyType} = scope.transaction;
      compileBasic({transaction: {email, currencyType}});
      expect(text).to.equal(`Sending money to ${email}`);
    });

    it(`email, amount, and currency`, () => {
      const {email, amount} = scope.transaction;
      compileBasic({transaction: {email, amount, currencyType: 'EUR'}});
      expect(text).to.equal(`Sending ${currency(amount, '€')} to ${email}`);
    });

    it(`email, amount, currency, and business purpose`, () => {
      const {email, amount} = scope.transaction;
      compileBasic({transaction: {email, amount, currencyType: 'JPY', purpose: 'GOODS_OR_SERVICES'}});
      expect(text).to.equal(`Sending ${currency(amount, '¥')} to ${email} for Goods or Services`);
    });

    it(`email, amount, currency, and family purpose`, () => {
      const {email, amount} = scope.transaction;
      compileBasic({transaction: {email, amount, currencyType: 'USD', purpose: 'FAMILY_OR_FRIEND'}});
      expect(text).to.equal(`Sending ${currency(amount, '$')} to ${email}, a family member or a friend`);
    });


    function compileAndDigest(template = basicTemplate, extraProps = {}) {
      _.assign(scope, extraProps);
      el = $compile(template)(scope);
      node = el[0];
      scope.$digest();
      text = node.textContent.trim().replace(/\n/, ' ').replace(/\s+/g, ' ');
    }
  });
};

