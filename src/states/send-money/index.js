import createState from 'createState';
import components from './components';
import formly from 'angular-formly';
import ngCommon from 'ngCommon';



export default createState({
  name: 'sendMoney',
  url: '/send-money',
  template: '<sm-send-money></sm-send-money>',
  data: {
    components,
    dependencies: [ngCommon, formly]
  }
});

