
// you'll notice in a few places I'm using
// ES6 modules and in others I'm using
// CommonJS. I generally try to use what's
// most convenient at the time. I expect to
// be transpiling until the day I die, so
// I don't really have a problem with doing
// stuff that may not be entirely "standard"
// Besides, I recently heard that you can write
// System module loaders that can consume
// CommonJS, so that's totally legit :-)

import 'createState';

const states = [
  require('./home'),
  require('./send-money'),
  require('./transactions')
];

export default states;

