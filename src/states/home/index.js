import createState from 'createState';
import components from './components';
import uiRouter from 'angular-ui-router';
import ngCommon from 'ngCommon';


export default createState({
  name: 'home',
  url: '/',
  template: '<sm-home></sm-home>',
  data: {
    components,
    dependencies: [uiRouter, ngCommon]
  }
});

