import _ from 'lodash';
export default ngModule => {
  describe('sm-home', () => {
    beforeEach(window.module(ngModule.name));

    let $compile, scope, el, node, $state;
    const basicTemplate = `<sm-home></sm-home>`;

    beforeEach(inject((_$compile_, $rootScope, _$state_) => {
      $compile = _$compile_;
      scope = $rootScope.$new();
      $state = _$state_;
      $state.go = sinon.spy();
    }));

    describe(`first button`, () => {
      it(`should send you to the send-money page`, () => {
        compileAndDigest();
        node.querySelectorAll('button')[0].click();
        expect($state.go).to.have.been.calledWith('sendMoney');
      });
    });

    describe(`second button`, () => {
      it(`should send you to the transactions page`, () => {
        compileAndDigest();
        node.querySelectorAll('button')[1].click();
        expect($state.go).to.have.been.calledWith('transactions');
      });
    });

    function compileAndDigest(template = basicTemplate, extraProps = {}) {
      _.assign(scope, extraProps);
      el = $compile(template)(scope);
      node = el[0];
      scope.$digest();
    }
  });
};

