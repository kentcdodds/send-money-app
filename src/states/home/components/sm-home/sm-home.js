import styles from './sm-home.css';
import smDirective from 'smDirective';
import res from 'resources';
import smLastTransactionFactory from './sm-last-transaction';

export default ngModule => {
  ngModule.directive('smHome', smHome);
  smLastTransactionFactory(ngModule);

  smHome.filename = __filename;

  /* istanbul ignore next */
  if (process.env.NODE_ENV === 'test') {
    require('./sm-home.test')(ngModule);
  }

  function smHome() {
    return smDirective({
      restrict: 'E',
      template: `
        <div class="sm-home +text-align-center ${styles.root}">
          <div class="${styles.container}">
            <img src="${res.images.logo.large}" class="${styles.logo}" />
            <sm-last-transaction
              ng-if="vm.transaction"
              transaction="vm.transaction"
            >
            </sm-last-transaction>
            <h1 class="x-large ${styles.title}">
              What are we {{vm.transaction ? 'doing now?' : 'doing?'}}
            </h1>
            <div class="+display-flex +flex-column">
              <div class="+flex-1 ${styles.buttonContainer}">
                <button class="btn" ng-click="vm.onSendMoneyClick()">
                  Send Money
                </button>
              </div>
              <div class="+flex-1 ${styles.buttonContainer}">
                <button class="btn" ng-click="vm.onViewTransactionHistoryClick()">
                  View Transaction History
                </button>
              </div>
            </div>
          </div>
        </div>
      `,
      scope: {},
      controllerAs: 'vm',
      bindToController: true,
      controller: SmHomeCtrl
    });

    // @ngInject
    function SmHomeCtrl($state, lastTransaction) {
      const vm = this;
      vm.transaction = lastTransaction.transaction;

      vm.onSendMoneyClick = onSendMoneyClick;
      vm.onViewTransactionHistoryClick = onViewTransactionHistoryClick;

      function onSendMoneyClick() {
        $state.go('sendMoney');
      }

      function onViewTransactionHistoryClick() {
        $state.go('transactions');
      }
    }
  }
};

