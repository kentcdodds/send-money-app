import styles from './sm-last-transaction.css';
import smDirective from 'smDirective';
import {noop} from 'angular';
export default ngModule => {
  ngModule.directive('smLastTransaction', smLastTransaction);

  /* istanbul ignore next */
  if (process.env.NODE_ENV === 'test') {
    require('./sm-last-transaction.test')(ngModule);
  }

  function smLastTransaction() {
    return smDirective({
      restrict: 'E',
      template: `
        <div class="sm-last-transaction">
          <div class="${styles.title}">
            <strong>Your last transaction was a success!</strong>
          </div>
          <sm-transaction-details
            transaction="vm.transaction"
          >
          </sm-transaction-details>
        </div>
      `,
      scope: {
        transaction: '='
      },
      controllerAs: 'vm',
      bindToController: true,
      controller: noop
    });
  }
};

