import _ from 'lodash';
export default ngModule => {
  describe('sm-transactions', () => {
    beforeEach(window.module(ngModule.name));

    let $compile, scope, el, node, isolateScope, vm;
    const basicTemplate = `<sm-transactions></sm-transactions>`;

    beforeEach(inject((_$compile_, $rootScope) => {
      $compile = _$compile_;
      scope = $rootScope.$new();
    }));

    it(`should compile`, () => {
      compileAndDigest();
      expect(node.className).to.contain('ng-scope');
      expect(node.querySelector('.sm-transactions')).to.exist;
      expect(vm).to.exist;
    });

    function compileAndDigest(template = basicTemplate, extraProps = {}) {
      _.assign(scope, extraProps);
      el = $compile(template)(scope);
      node = el[0];
      scope.$digest();
      isolateScope = el.isolateScope();
      vm = isolateScope.vm;
    }
  });
};

