import styles from './sm-transaction-row.css';
import smDirective from 'smDirective';
import smTransactionHeaderFactory from './sm-transaction-header';
import {noop} from 'angular';

export default ngModule => {
  ngModule.directive('smTransactionRow', smTransactionRow);
  smTransactionHeaderFactory(ngModule);

  /* istanbul ignore next */
  if (process.env.NODE_ENV === 'test') {
    require('./sm-transaction-row.test')(ngModule);
  }

  function smTransactionRow() {
    return smDirective({
      restrict: 'E',
      template: `
        <div class="sm-transaction-row">
          <sm-transaction-header
            class="+cursor-pointer"
            ng-click="vm.show=!vm.show"
            transaction="vm.transaction"
          >
          </sm-transaction-header>
          <div class="${styles.details}" ng-if="vm.show">
            <sm-transaction-details
              transaction="vm.transaction"
            >
            </sm-transaction-details>
          </div>
        </div>
      `,
      scope: {
        transaction: '='
      },
      controllerAs: 'vm',
      bindToController: true,
      controller: noop
    });
  }
};

