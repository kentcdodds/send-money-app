import styles from './sm-transaction-header.css';
import smDirective from 'smDirective';
export default ngModule => {
  ngModule.directive('smTransactionHeader', smTransactionHeader);

  smTransactionHeader.filename = __filename;

  /* istanbul ignore next */
  if (process.env.NODE_ENV === 'test') {
    require('./sm-transaction-header.test')(ngModule);
  }

  function smTransactionHeader() {
    return smDirective({
      restrict: 'E',
      template: `
        <div class="sm-transaction-header transactionItem">
          <div class="+display-flex">
            <div class="+flex-1">
              <div class="dateParts +text-align-center">
                <span class="dateMonth vx_h5">{{::vm.transaction.date | date:'MMM'}}</span>
                <span class="dateDay vx_h4">{{::vm.transaction.date | date:'dd'}}</span>
                <span class="dateYear vx_h7">{{::vm.transaction.date | date:'yyyy'}}</span>
              </div>
            </div>
            <div class="+flex-11 transactionDetailsContainer ${styles.detailsContainer}">
              <a class="transactionDescriptionContainer" role="button">
                <span class="transactionDescription ${styles.description}">{{::vm.transaction.email}}</span>
                <span class="transactionType vx_small-text ${styles.purpose}">
                  {{::vm.transaction.purpose | smPurpose}}
                </span>
              </a>
              <div class="transactionAmount primaryCurrency">
                <span
                  class="vx_h3"
                  ng-class="{'isNegative': vm.transaction.amount < 0, 'isPositive': vm.transaction.amount > 0}"
                >
                  {{vm.transaction.amount > 0 ? '+' : '-'}}
                </span>
                <span class="netAmount vx_h4">
                  {{::vm.absValOfTransaction | smCurrency:vm.transaction.currencyType}}
                </span>
              </div>
            </div>
          </div>
        </div>
      `,
      scope: {
        transaction: '='
      },
      scopeTypes,
      controllerAs: 'vm',
      bindToController: true,
      controller: SmTransactionHeaderCtrl
    });

    // @ngInject
    function SmTransactionHeaderCtrl() {
      const vm = this;

      // we let the pretty green and red symbols cover the positive/negative for us
      vm.absValOfTransaction = Math.abs(vm.transaction.amount);
    }

    /* istanbul ignore next */
    function scopeTypes(check) {
      return {
        transaction: check.shape({
          email: check.string,
          amount: check.number,
          currencyType: check.string,
          purpose: check.string,
          date: check.string,
        })
      };
    }
  }
};

