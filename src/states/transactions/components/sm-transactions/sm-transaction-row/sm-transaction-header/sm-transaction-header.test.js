import _ from 'lodash';
import {models} from '@kentcdodds/send-money-common';

const mockTransaction = models.transaction.mock;

export default ngModule => {
  describe('sm-transaction-header', () => {
    beforeEach(window.module(ngModule.name));

    let $compile, scope, el, node, isolateScope, vm;
    const basicTemplate = `
      <sm-transaction-header transaction="transaction">
      </sm-transaction-header>
    `;

    beforeEach(inject((_$compile_, $rootScope) => {
      $compile = _$compile_;
      scope = $rootScope.$new();
      scope.transaction = mockTransaction();
    }));

    it(`should compile`, () => {
      compileAndDigest();
      expect(node.className).to.contain('ng-scope');
      expect(node.querySelector('.sm-transaction-header')).to.exist;
      expect(vm).to.exist;
    });

    function compileAndDigest(template = basicTemplate, extraProps = {}) {
      _.assign(scope, extraProps);
      el = $compile(template)(scope);
      node = el[0];
      scope.$digest();
      isolateScope = el.isolateScope();
      vm = isolateScope.vm;
    }
  });
};

