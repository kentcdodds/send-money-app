import styles from './sm-transactions.css';
import smTransactionRowFactory from './sm-transaction-row';
import smDirective from 'smDirective';

export default ngModule => {
  ngModule.directive('smTransactions', smTransactions);
  smTransactionRowFactory(ngModule);

  /* istanbul ignore next */
  if (process.env.NODE_ENV === 'test') {
    require('./sm-transactions.test')(ngModule);
  }

  function smTransactions() {
    return smDirective({
      restrict: 'E',
      template: `
        <div class="sm-transactions">
          <sm-page-title title="Transactions" class="${styles.title}"></sm-page-title>
          <ul
            infinite-scroll="vm.loadMoreTransactions()"
            infinite-scroll-distance="1"
            infinite-scroll-disabled="vm.infiniteScrollDisabled"
            class="transactionList ${styles.list}"
          >
            <li
              class="transactionRow transactionRowHover"
              ng-repeat="transaction in vm.transactions track by transaction._id"
            >
             <sm-transaction-row transaction="transaction">
             </sm-transaction-row>
            </li>
          </ul>
        </div>
      `,
      scope: {
        transactions: '=',
        count: '=',
        offset: '='
      },
      scopeTypes,
      controllerAs: 'vm',
      bindToController: true,
      controller: SmTransactionsCtrl
    });

    // @ngInject
    function SmTransactionsCtrl(sendMoneyAPI) {
      const vm = this;

      vm.loadMoreTransactions = loadMoreTransactions;

      function loadMoreTransactions() {
        vm.offset += 20;
        vm.infiniteScrollDisabled = true;
        vm.loading = sendMoneyAPI.getTransactions(vm.offset, vm.count)
          .then(transactions => {
            if (transactions.length) {
              vm.transactions = [...vm.transactions, ...transactions];
              vm.infiniteScrollDisabled = false;
            } else {
              vm.allLoaded = true;
              // all transactions loaded
            }
          });
      }

    }

    /* istanbul ignore next */
    function scopeTypes(check) {
      return {
        transactions: check.arrayOf(check.shape({
          amount: check.number
        }))
      };
    }
  }
};

