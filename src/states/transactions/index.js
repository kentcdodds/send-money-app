import './styles';
import ngCommon from 'ngCommon';
import createState from 'createState';
import components from './components';
import 'ng-infinite-scroll'; // <-- they don't export the name :-(
const ngInfiniteScroll = 'infinite-scroll';

export default createState({
  name: 'transactions',
  url: '/transactions',
  template: `
    <sm-transactions
      transactions="vm.transactions"
      count="vm.count"
      offset="vm.offset"
    >
    </sm-transactions>
  `,
  data: {
    components,
    dependencies: [ngCommon, ngInfiniteScroll]
  },
  resolve: {
    transactions,
    count: resolveIdentity(20),
    offset: resolveIdentity(0)
  }
});

// @ngInject
function transactions(sendMoneyAPI) {
  return sendMoneyAPI.getTransactions();
}

function resolveIdentity(identity) {
  return () => identity;
}

