import './css';
import angular from 'angular';
import getStateConfigurer from './getStateConfigurer';
import states from '../states';

const ngModuleName = 'smApp';

export default ngModuleName;

const stateConfig = getStateConfigurer(states);
const stateModules = states.map(state => state.data.ngModule.name);

angular
  .module(ngModuleName, stateModules)
  .config(stateConfig);

