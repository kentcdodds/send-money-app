export default getStateConfigurer;

function getStateConfigurer(states) {
  return config;

  // @ngInject
  function config($urlRouterProvider, $compileProvider, $httpProvider, $stateProvider) {
    // disable debug info for improved perf when not in development
    $compileProvider.debugInfoEnabled(process.env.NODE_ENV === 'development');

    // batch up $digests for $http calls
    $httpProvider.useApplyAsync();

    states.forEach(state => $stateProvider.state(state));

    // redirect unhandled urls to the root
    $urlRouterProvider.otherwise('/');
  }
}

