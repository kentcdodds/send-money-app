/*
 * This is a file that I developed in one of my side-projects
 * It's been really helpful to me elsewhere so I've incldued it here
 */


/*
 * This is an excluded file, meaning it will be loaded in dev, but excluded in the prod build. It has utilities that are
 * awesome.
 */
/* eslint devel: 0 */
/* eslint max-statements: 0 */
/* eslint complexity: 0 */
/* eslint no-console: 0 */
/* eslint valid-jsdoc: 0 */
/* eslint no-invalid-this: 0 */

import angular from 'angular';

// Angular utils
window.smApp = (function smAppGetter() {

  /**
   * Sometimes you have two scopes that are supposedly related,
   * but you don't know how far back to go before you find that relation
   *
   * Usage:
   *
   * const closestParent = closestScopeParent(scope1, scope2);
   * // or
   * const closestParent = closestScopeParent('003', '0M4');
   * @param scope1
   * @param scope2
   * @returns {*}
   */
  function closestScopeParent(scope1, scope2) {
    scope1 = makeScopeReference(scope1);
    scope2 = makeScopeReference(scope2);
    let bump1 = false;
    while (scope1 !== scope2 && scope1 && scope2) {
      if (bump1) {
        scope1 = scope1.$parent;
      } else {
        scope2 = scope2.$parent;
      }
      bump1 = !bump1;
    }
    return scope1;
  }

  closestScopeParent.help = 'Find the lowest common ancestor of two scopes. Provide scopes by reference or id.';

  /**
   * Sometimes you have a scope ID and you have no idea what element
   * that scope belongs to, use this to find the element.
   *
   * Usage:
   *
   * const element = getElementByScopeId('003');
   * // NOTE: Some scopes don't have an element (apparently)...
   * @param scope
   * @returns {*}
   */
  function getElementByScope(scope) {
    if (Array.isArray(scope)) {
      return scope.map(s => getElementByScope(s));
    }
    scope = makeScopeReference(scope);
    const ngScopes = getAllScopeElements();
    for (let i = 0; i < ngScopes.length; i++) {
      const element = angular.element(ngScopes.item(i));
      const aScope = element.scope();
      const isolateScope = element.isolateScope();
      if ((aScope && scope.$id === aScope.$id) || (isolateScope && scope.$id === isolateScope.$id)) {
        return element;
      }
    }
  }

  getElementByScope.help = 'Find the root element of a scope by the scope\'s reference or id.';

  /**
   * Scopes are on elements with the .ng-scope class on them.
   * @returns {NodeList}
   */
  function getAllScopeElements() {
    return document.documentElement.querySelectorAll('.ng-scope');
  }

  getAllScopeElements.help = 'Find the root element of all scopes on the page.';

  function getAllScopes(scope) {
    const scopes = [];
    iterateScopes(scope, scopes.push.bind(scopes));
    return scopes;
  }

  getAllScopes.help = 'Get all the scopes on the page as an array';
  getAllScopes.args = ['The scope to start on (optional, defaults to $rootScope)'];

  /**
   * When you have an id for a scope, but not the scope itself.
   * Usage:
   * const scope25 = getScopeById(25);
   * @param id
   * @returns {*}
   */
  function getScopeById(id) {
    let myScope = null;
    iterateScopes(scope => {
      if (scope.$id === id) {
        myScope = scope;
        return false;
      }
    });
    return myScope;
  }

  getScopeById.help = 'Get a reference to a scope by the id of the scope';

  /**
   * Sometimes you want to do the same thing to all scopes on a page, or find
   * which scope has a specific property.
   * Provide the starting scope (or it will use the $rootScope) and it'll
   * iterate through all scopes below starting with the given scope
   *
   * Usage:

   * iterateScopes($scope, function(scope) {
     *   console.log(scope);
     * });

   * iterateScopes(function(scope) {
     *   console.log(scope);
     * });

   * iterateScopes(function(scope) {
     *   if (scope.hasOwnProperty('propOfInterest')) {
     *     debugger;
     *     // so you can do stuff with that scope
     *   }
     * });

   * const coolScope;
   * iterateScopes(function(scope) {
     *   if (scope.isCool) {
     *     coolScope = scope;
     *     return false; // early exit
     *   }
     * });
   * @param current
   * @param fn
   * @returns {*}
   */
  function iterateScopes(current, fn) {
    if (typeof current === 'function') {
      fn = current;
      current = null;
    }
    current = current || getRootScope();
    current = makeScopeReference(current);
    const ret = fn(current);
    if (ret === false) {
      return ret;
    }
    return iterateChildren(current, fn);
  }

  iterateScopes.help = 'Iterate through all scopes on the page.';
  iterateScopes.args = [
    'the scope to start on (optional, and can be id)',
    'the function to invoke with the scopes: fn(scope)'
  ];

  function iterateSiblings(start, fn) {
    let ret;
    /* eslint no-cond-assign:0 */
    while (start = start.$$nextSibling) {
      ret = fn(start);
      if (ret === false) {
        break;
      }

      ret = iterateChildren(start, fn);
      if (ret === false) {
        break;
      }
    }
    return ret;
  }

  function iterateChildren(start, fn) {
    let ret;
    /* eslint no-cond-assign:0 */
    while (start = start.$$childHead) {
      ret = fn(start);
      if (ret === false) {
        break;
      }

      ret = iterateSiblings(start, fn);
      if (ret === false) {
        break;
      }
    }
    return ret;
  }

  function getScopesWithProperty(propertyName, start) {
    const scopes = [];
    iterateScopes(start, scope => {
      if (angular.isDefined(scope.$eval(propertyName))) {
        scopes.push(scope);
      }
    });
    return scopes;
  }

  getScopesWithProperty.help = 'Get all scopes where the given expression evaluates to defined';

  /**
   * Get the root scope.
   * @returns {*}
   */
  function getRootScope() {
    const firstElementWithScope = document.documentElement.querySelector('.ng-scope');
    const scope = angular.element(firstElementWithScope).scope();
    return scope.$root;
  }

  getRootScope.help = 'Get the $rootScope';

  /**
   * Get watcher count for an element and all of its children
   * @param element
   * @returns {number}
   */
  function getWatcherCount(element) {
    element = getElement(element);

    let watcherCount = 0;
    const isolateWatchers = getWatchersFromScope(element.data().$isolateScope);
    const scopeWatchers = getWatchersFromScope(element.data().$scope);
    const watchers = scopeWatchers.concat(isolateWatchers);
    watcherCount += watchers.length;
    angular.forEach(element.children(), childElement => {
      watcherCount += getWatcherCount(angular.element(childElement));
    });
    return watcherCount;
  }

  getWatcherCount.help = 'Get the current watch count from an element and its children';
  getWatcherCount.args = ['the element to start on (optional: defaults to documentElement)'];

  function getWatchersFromScope(scope) {
    scope = makeScopeReference(scope);
    return scope && scope.$$watchers ? scope.$$watchers : [];
  }

  getWatchersFromScope.help = 'Get the given scope\'s $$watchers';
  getWatchersFromScope.args = ['scope by reference or id'];

  function scopeIsRoot(scope) {
    scope = makeScopeReference(scope);
    return scope && scope.$root === scope;
  }

  scopeIsRoot.help = 'Returns true if the given scope (or scope id) is equal to $rootScope';

  function help(fn) {
    let message, args, theFn;
    const fns = Object.keys(this);
    if (fn && !this[fn] && !fns[fn]) {
      throw new Error('This has no function called ' + fn);
    } else if (typeof fn !== 'undefined') {
      theFn = this[fn] || this[fns[fn]];
      message = theFn.name + ': ' + (theFn.help || 'no help available') + '\n';
      args = getDependencies(theFn);
      if (args.length) {
        message += 'Parameters:\n\t';
      } else {
        message += 'No parameters';
      }

      message += args.map((arg, index) => {
        return '[' + index + '] ' + args[index] + (theFn.args ? ': ' + theFn.args[index] : '');
      }).join('\n\t');

      console.log(message);
      return;
    }

    message = fns.map((key, index) => {
      return '[' + index + '] ' + key + ': ' + this[key].help || '';
    });
    console.log(message.join('\n'));
  }

  help.help = 'Shows this help output. Pass the name or number of a function for more info.';
  help.args = [
    'The name or number of a function to get extra info'
  ];


  return {
    closestScopeParent,
    getScopeById,
    getElementByScope,
    getAllScopeElements,
    getAllScopes,
    iterateScopes,
    getWatcherCount,
    getWatchersFromScope,
    getRootScope,
    scopeIsRoot,
    getScopesWithProperty,
    help
  };

  // UTILS

  function makeScopeReference(scope) {
    if (isScopeId(scope)) {
      scope = getScopeById(scope);
    }
    return scope;
  }

  function isScopeId(scope) {
    return typeof scope === 'string' || typeof scope === 'number';
  }

  function getDependencies(func) {
    // strip comments
    const fnStr = func.toString().replace(/((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg, '');
    // get argument names
    const result = fnStr.slice(fnStr.indexOf('(') + 1, fnStr.indexOf(')')).match(/([^\s,]+)/g);
    return result || [];
  }

  function getElement(el) {
    if (!el) {
      el = document.documentElement;
    }
    return angular.element(el);
  }

})();

console.log('smApp utils installed, run smApp.help() for more information');


const intervalId = setInterval(() => {
  const firstScopeEl = document.querySelector('.ng-scope');
  const $injector = angular.element(firstScopeEl).injector();
  if (!$injector) {
    return;
  }
  clearInterval(intervalId);
  window.$injector = $injector;
  const injectables = [
    '$rootScope', '$http', '$state', '$stateParams', '$location',
    '$log', '$timeout', 'formlyConfig', 'formlyVersion',
    '$parse', '$templateCache', '$filter', '$compile'
  ];
  injectables.forEach(injectable => {
    try {
      window[injectable] = $injector.get(injectable);
    } catch (e) {
      console.log('Cannot get injectable: ', injectable, '. You may want to remove this from dev.excluded.js');
    }
  });

  // add stuff to $rootScope that make debugging in templates nicer
  window.$rootScope.onDev = true;
  window.i = $injector.get.bind($injector);
  console.log('smConfig', window.smConfig);
}, 10); // wait for bootstrap


// window errors
window.onerror = function onerror(errorMsg, url, lineNumber, columnNumber, errorObject) {
  if (/<omitted>/.test(errorMsg)) {
    console.error('Error: ' + errorObject ? errorObject.message : errorMsg);
  }
};

