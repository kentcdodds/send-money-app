import angular from 'angular';
import ngModuleName from '../app';
export default onReady;

/* istanbul ignore next */
if (process.env.NODE_ENV === 'test') {
  require('./bootstrap.test')(onReady, ngModuleName);
}

function onReady() {
  const start = Date.now();

  /* istanbul ignore next */
  if (process.env.NODE_ENV === 'development') {
    require('../dev/dev.excluded');
    window.showAngularStats = require('ng-stats');
    try {
      require('../dev/custom.ignored');
    } catch (e) {
      /* eslint no-empty:0 */ // ignore this case. There's simply no custom.ignored.
    }
  }


  angular
    .module(ngModuleName)
    .run(removeLoadingMessage);

  /*
   * It may seem like overkill to have all of this stuff just to remove the loading message
   * But it's important to me that when users initally load the site, they see something
   * indicating that the site is loading. Especially with high network latency.
   * So even though this is a tad bit complex, it's worth it.
   */
  // @ngInject
  function removeLoadingMessage($rootScope, $state, $log, $rootElement) {
    const stopListening = [];
    stopListening.push($rootScope.$on('$stateChangeSuccess', removeLoading));
    stopListening.push($rootScope.$on('$stateChangeError', removeLoading));
    stopListening.push($rootScope.$on('$stateNotFound', removeLoading));

    function removeLoading({name}) {
      stopListening.forEach(stop => stop());
      $log.info(`${name}... removing loading message`);
      angular.element($rootElement[0].querySelector('#app-loading-message')).remove();
      const appFinishedLoading = Date.now();
      if (window.loadStart) {
        $log.info(`Start to finish, it took ${appFinishedLoading - window.loadStart}ms to load...`);
        window.loadStart = undefined;
      } else {
        $log.warn('This should never happen. window.loadStart was not ever defined');
      }
    }
  }

  /* istanbul ignore next */
  if (process.env.NODE_ENV !== 'test') {
    // bootstrap our module
    angular.bootstrap(document.body, [ngModuleName], {strictDi: true});
    const end = Date.now();
    // it's handy to keep track of how long it takes to bootstrap
    // if this were a real app deployed to production, we'd want
    // to push this info to analytics.
    console.log(`send-money app bootstrapped in ${end - start}ms.`); // eslint-disable-line no-console
  }
}
