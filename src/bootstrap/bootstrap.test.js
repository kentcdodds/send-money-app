import {bootstrap} from 'test/utils.test';

/*
 * This is not a normal angular test file.
 * We are testing the logic around bootstrapping
 * the application to make sure that we never ship
 * code which causes our app to fail bootstrapping
 *
 * A better way to ensure this is to set up e2e tests
 * where the app is actually loaded. We'll see if I
 * get to that :-)
 *
 * But because we're testing the bootstrap phase,
 * we can't use angular-mock's helpers, which is
 * why if you're familiar with angular, this file
 * looks slightly different from what you're used to
 */

export default (onReady, ngModuleName) => {
  describe(`bootstrap`, () => {
    beforeEach(() => {
      window.loadStart = new Date();
    });

    it(`should remove the loading message`, () => {
      onReady();
      const {node} = bootIt();
      expect(
        node.querySelector('#app-loading-message'),
        'app-loading-message element'
      ).to.be.null;
    });

    it(`should warn if window.loadStart is not defined`, () => {
      window.loadStart = undefined;
      onReady();
      const {$log} = bootIt(['$log']);
      expect($log.warn).to.have.been.calledWith('This should never happen. window.loadStart was not ever defined');
    });

  });

  function bootIt(stuffToGet) {
    const innerHTML = `<div id="app-loading-message"></div>`;
    return bootstrap(stuffToGet, {ngModuleName, innerHTML});
  }


};
