// pull out our global deps up here
// so we're a little more explicit
// about our implicit dependencies
const {API_BASE_URL} = window.smConfig;

export default ngModule => {
  ngModule.factory('sendMoneyAPI', sendMoneyAPI);

  sendMoneyAPI.filename = __filename;

  /* istanbul ignore next */
  if (process.env.NODE_ENV === 'test') {
    require('./sendMoneyAPI.test')(ngModule);
  }

  function sendMoneyAPI($http) {
    return {
      getTransactions,
      addTransaction
    };

    function getTransactions(offset = 0, count = 20) {
      return request('transactions', {
        version: 'v1',
        params: {offset, count}
      });
    }

    function addTransaction(transaction) {
      return request('transactions', {
        method: 'POST',
        version: 'v1',
        data: transaction
      });
    }

    function request(url, config) {
      return $http({
        url: `${API_BASE_URL}/${config.version}/${url}`,
        method: 'GET',
        ...config
      }).then(res => res.data);
    }
  }
};

