import _ from 'lodash';
import {models} from '@kentcdodds/send-money-common';
const mockTransaction = models.transaction.mock;

export default ngModule => {
  describe('sendMoneyAPI', () => {
    beforeEach(window.module(ngModule.name));

    let $httpBackend, sendMoneyAPI;

    beforeEach(inject((_sendMoneyAPI_, _$httpBackend_) => {
      sendMoneyAPI = _sendMoneyAPI_;
      $httpBackend = _$httpBackend_;
    }));

    describe(`getTransactions`, () => {

      it(`should hit the correct endpoint`, () => {
        const randomTransactions = _.times(20, mockTransaction);
        $httpBackend
          .expectGET('https://example.com/api/v1/transactions?count=20&offset=0')
          .respond(randomTransactions);
        sendMoneyAPI.getTransactions();
        $httpBackend.flush();
      });
    });

    describe(`addTransaction`, () => {

      it(`should hit the correct endpoint`, () => {
        const randomTransaction = mockTransaction();
        $httpBackend
          .expectPOST('https://example.com/api/v1/transactions')
          .respond(randomTransaction);
        sendMoneyAPI.addTransaction(randomTransaction);
        $httpBackend.flush();
      });
    });



    afterEach('verify $httpBackend is clean', () => {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

  });
};

