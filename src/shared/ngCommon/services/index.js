export default ngModule => {
  require('./sendMoneyAPI')(ngModule);
  require('./smUtils')(ngModule);
  require('./lastTransaction')(ngModule);
};
