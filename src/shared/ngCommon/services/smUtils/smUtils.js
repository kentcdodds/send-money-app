import {noop} from 'angular';

export default ngModule => {
  ngModule.factory('smUtils', smUtils);

  // TODO: test this

  function smUtils($timeout, $q) {
    return {promiseMinTime};

    function promiseMinTime(promise, milliseconds) {
      const timeoutPromise = $timeout(noop, milliseconds);
      return $q
        .all([promise, timeoutPromise])
        .then(results => results[0])
        .catch(onPromiseError);

      function onPromiseError(error) {
        return timeoutPromise.then(function throwOriginalPromiseError() {
          throw error;
        });
      }
    }
  }
};

