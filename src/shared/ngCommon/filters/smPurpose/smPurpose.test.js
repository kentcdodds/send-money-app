export default ngModule => {
  describe('smPurpose', () => {
    beforeEach(window.module(ngModule.name));

    let smPurpose;

    beforeEach(inject(($filter) => {
      smPurpose = $filter('smPurpose');
    }));

    it(`should work with GOODS_OR_SERVICES`, () => {
      expect(smPurpose('GOODS_OR_SERVICES')).to.equal('Goods or Services');
    });

    it(`should work with FAMILY_OR_FRIEND`, () => {
      expect(smPurpose('FAMILY_OR_FRIEND')).to.equal('Family or Friend');
    });
  });
};

