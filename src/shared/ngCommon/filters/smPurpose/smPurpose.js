import purposes from 'enums/purposes';

export default ngModule => {
  ngModule.filter('smPurpose', smPurposeFunction);

  smPurposeFunction.filename = __filename;

  /* istanbul ignore next */
  if (process.env.NODE_ENV === 'test') {
    smPurposeFunction.test = require('./smPurpose.test')(ngModule);
  }

  function smPurposeFunction() {
    return function smPurpose(input) {
      return purposes[input];
    };
  }
};

