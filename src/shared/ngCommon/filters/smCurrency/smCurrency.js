import currencyTypes from 'enums/currencyTypes';
export default ngModule => {
  ngModule.filter('smCurrency', smCurrencyFunction);

  /* istanbul ignore next */
  if (process.env.NODE_ENV === 'test') {
    smCurrencyFunction.test = require('./smCurrency.test')(ngModule);
  }

  function smCurrencyFunction($filter) {
    const currency = $filter('currency');
    return function smCurrency(input, unit) {
      return currency(input, currencyTypes[unit]);
    };
  }
};

