export default ngModule => {
  describe('smCurrency', () => {
    beforeEach(window.module(ngModule.name));

    let smCurrency;

    beforeEach(inject(($filter) => {
      smCurrency = $filter('smCurrency');
    }));

    it(`should work with USD`, () => {
      expect(smCurrency(23, 'USD')).to.equal('$23.00');
    });

    it(`should work with JPY`, () => {
      expect(smCurrency(233.2, 'JPY')).to.equal('¥233.20');
    });

    it(`should work with EUR`, () => {
      expect(smCurrency(0.23, 'EUR')).to.equal('€0.23');
    });
  });
};

