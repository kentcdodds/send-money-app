export default ngModule => {
  require('./sm-next-previous')(ngModule);
  require('./sm-page-title')(ngModule);
  require('./sm-spinner')(ngModule);
  require('./sm-transaction-details')(ngModule);
};
