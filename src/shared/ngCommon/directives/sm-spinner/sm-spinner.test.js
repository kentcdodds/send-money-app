import _ from 'lodash';
export default ngModule => {
  describe('sm-spinner', () => {
    beforeEach(window.module(ngModule.name));

    let $compile, scope, el, node;
    const basicTemplate = `<sm-spinner text="Hello World"></sm-spinner>`;

    beforeEach(inject((_$compile_, $rootScope) => {
      $compile = _$compile_;
      scope = $rootScope.$new();
    }));

    it(`should show text specified`, () => {
      compileAndDigest();
      expect(node.textContent.trim()).to.equal('Hello World');
    });

    function compileAndDigest(template = basicTemplate, extraProps = {}) {
      _.assign(scope, extraProps);
      el = $compile(template)(scope);
      node = el[0];
      scope.$digest();
    }
  });
};

