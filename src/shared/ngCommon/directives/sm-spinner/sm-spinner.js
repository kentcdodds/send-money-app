import smDirective from 'smDirective';
import {noop} from 'angular';
import styles from './sm-spinner.css';
export default ngModule => {
  ngModule.directive('smSpinner', smSpinner);

  /* istanbul ignore next */
  if (process.env.NODE_ENV === 'test') {
    require('./sm-spinner.test')(ngModule);
  }

  function smSpinner() {
    return smDirective({
      restrict: 'E',
      template: `
        <div class="sm-spinner">
          <div class="${styles.spinner}"></div>
          {{vm.text}}
        </div>
      `,
      scope: {
        text: '@'
      },
      controllerAs: 'vm',
      bindToController: true,
      controller: noop
    });
  }
};

