import styles from './sm-transaction-details.css';
import smDirective from 'smDirective';
import {noop} from 'angular';
export default ngModule => {
  ngModule.directive('smTransactionDetails', smTransactionDetails);

  /* istanbul ignore next */
  if (process.env.NODE_ENV === 'test') {
    require('./sm-transaction-details.test')(ngModule);
  }

  function smTransactionDetails() {
    return smDirective({
      restrict: 'E',
      template: `
        <div class="sm-transaction-details">
          <div class="+display-flex +flex-center">
            <div class="${styles.labels}">
              To:<br />
              Amount:<br />
              Purpose:<br />
              Message:<br />
            </div>
            <div class="${styles.values}">
              {{vm.transaction.email}}<br />
              {{vm.transaction.amount | smCurrency:vm.transaction.currencyType}}<br />
              {{vm.transaction.purpose | smPurpose}}<br />
              {{vm.transaction.message}}
            </div>
          </div>
        </div>
      `,
      scope: {
        transaction: '='
      },
      scopeTypes,
      controllerAs: 'vm',
      bindToController: true,
      controller: noop
    });

    /* istanbul ignore next */
    function scopeTypes(check) {
      return {
        transaction: check.object
      };
    }
  }
};

