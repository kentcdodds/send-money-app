import _ from 'lodash';
export default ngModule => {
  describe('sm-page-title', () => {
    beforeEach(window.module(ngModule.name));

    let $compile, scope, el, node, text;
    const basicTemplate = `<sm-page-title title="Hello world!"></sm-page-title>`;

    beforeEach(inject((_$compile_, $rootScope) => {
      $compile = _$compile_;
      scope = $rootScope.$new();
    }));

    it(`should compile`, () => {
      compileAndDigest();
      expect(node.querySelector('img')).to.exist;
      expect(text).to.equal('Go home Hello world!');
    });

    function compileAndDigest(template = basicTemplate, extraProps = {}) {
      _.assign(scope, extraProps);
      el = $compile(template)(scope);
      node = el[0];
      scope.$digest();
      text = node.textContent.trim().replace(/\n/, ' ').replace(/\s+/g, ' ');
    }
  });
};

