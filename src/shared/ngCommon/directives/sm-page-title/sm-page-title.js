import res from 'resources';
import styles from './sm-page-title.css';
import {noop} from 'angular';
import smDirective from 'smDirective';

export default ngModule => {
  ngModule.directive('smPageTitle', smPageTitle);

  /* istanbul ignore next */
  if (process.env.NODE_ENV === 'test') {
    require('./sm-page-title.test')(ngModule);
  }

  function smPageTitle() {
    return smDirective({
      restrict: 'E',
      replace: true,
      template: `
        <div class="sm-page-title ${styles.titleContainer}">
          <a ui-sref="home"><img src="${res.images.logo.medium}" class="${styles.logo}" /></a>
          <br />
          <a ui-sref="home">Go home</a>
          <h2>{{::vm.title}}</h2>
        </div>
      `,
      scope: {
        title: '@'
      },
      scopeTypes,
      controllerAs: 'vm',
      bindToController: true,
      controller: noop
    });

    /* istanbul ignore next */
    function scopeTypes(check) {
      return {
        title: check.string
      };
    }
  }
};

