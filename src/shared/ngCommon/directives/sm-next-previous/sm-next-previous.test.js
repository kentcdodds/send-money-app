import _ from 'lodash';
export default ngModule => {
  describe('sm-next-previous', () => {
    beforeEach(window.module(ngModule.name));

    let $compile, scope, el, node, next, prev;
    const basicTemplate = `
      <sm-next-previous
        state="state"
        stage-valid="stageValid"
        max="4"
      >
      </sm-next-previous>
    `;

    beforeEach(inject((_$compile_, $rootScope) => {
      $compile = _$compile_;
      scope = $rootScope.$new();
      scope.state = {stage: 0};
      scope.stageValid = true;
    }));

    it(`should hide the previous button when the stage is <= 0`, () => {
      compileAndDigest();
      expect(prev).to.not.exist;
    });

    it(`should hide the next button when the stage is > max`, () => {
      compileAndDigest(undefined, {
        state: {stage: 4}
      });
      expect(next).to.not.exist;
    });

    it(`should increment the state.stage when the next button is clicked`, () => {
      compileAndDigest();
      next.click();
      expect(scope.state.stage).to.equal(1);
    });

    it(`should decrement the state.stage when the previous button is clicked`, () => {
      compileAndDigest(undefined, {
        state: {stage: 4}
      });
      prev.click();
      expect(scope.state.stage).to.equal(3);
    });


    it(`should disable the next button if the stage is invalid`, () => {
      compileAndDigest(undefined, {
        state: {stage: 3}, stageValid: false
      });
      expect(next.disabled).to.be.true;
    });

    function compileAndDigest(template = basicTemplate, extraProps = {}) {
      _.assign(scope, extraProps);
      el = $compile(template)(scope);
      node = el[0];
      scope.$digest();
      prev = node.querySelector('._prev');
      next = node.querySelector('._next');
    }
  });
};

