import smDirective from 'smDirective';

export default ngModule => {
  ngModule.directive('smNextPrevious', smNextPrevious);

  /* istanbul ignore next */
  if (process.env.NODE_ENV === 'test') {
    require('./sm-next-previous.test')(ngModule);
  }

  function smNextPrevious() {
    return smDirective({
      restrict: 'E',
      template: `
        <div class="sm-next-previous">
          <button
            ng-if="vm.state.stage > 0"
            ng-click="vm.decrement()"
            class="btn _prev"
            type="button"
          >
            Previous
          </button>
          <button
            ng-if="vm.state.stage < vm.max"
            ng-click="vm.increment()"
            ng-disabled="!vm.stageValid"
            class="btn _next"
            type="button"
          >
            Next
          </button>
        </div>
      `,
      scope: {
        state: '=',
        stageValid: '=',
        max: '='
      },
      scopeTypes,
      controllerAs: 'vm',
      bindToController: true,
      controller: SmNextPreviousCtrl
    });

    // @ngInject
    function SmNextPreviousCtrl() {
      const vm = this;

      vm.increment = () => vm.state.stage++;
      vm.decrement = () => vm.state.stage--;
    }

    /* istanbul ignore next */
    function scopeTypes(check) {
      return {
        state: check.shape({
          stage: check.number
        }),
        stageValid: check.bool,
        max: check.number
      };
    }
  }
};

