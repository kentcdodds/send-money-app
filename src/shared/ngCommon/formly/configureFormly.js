import smApiCheck from 'smApiCheck';
import manipulators from './manipulators';


export default configureFormly;

// @ngInject
function configureFormly(formlyConfig, formlyValidationMessages) {
  formlyConfig.disableWarnings = process.env.NODE_ENV === 'production';
  const {extras} = formlyConfig;
  extras.removeChromeAutoComplete = true;
  extras.apiCheckInstance = smApiCheck;

  // general validation messages
  formlyValidationMessages.addStringMessage('required', 'Required');
  formlyValidationMessages.addStringMessage('number', 'Bad number');
  formlyValidationMessages.addStringMessage('email', 'Bad email');
  formlyValidationMessages.addStringMessage('min', 'Postive only');

  const {templateManipulators} = formlyConfig;
  addManipulators('preWrapper', manipulators.pre);
  addManipulators('postWrapper', manipulators.post);

  function addManipulators(type, array) {
    templateManipulators[type] = (templateManipulators[type] || []).concat(array || []);
  }
}
