import angular from 'angular';
import components from './components';

const ngModuleName = 'smFormly';

export default ngModuleName;

const ngModule = angular
  .module(ngModuleName, require('./types'))
  .run(require('./configureFormly'));

components(ngModule);

