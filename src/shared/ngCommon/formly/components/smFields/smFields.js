import styles from './smFields.css';
export default ngModule => {
  ngModule.factory('smFields', smFields);

  function smFields() {
    return {
      amountField
    };

    function amountField(focus, required, overrides) {
      return {
        fieldGroup: [
          {
            template: '',
            templateOptions: {
              label: 'How much will you be sending them?'
            },
          },
          {
            className: '+display-flex +flex-center',
            fieldGroup: [
              {
                type: 'input',
                key: 'amount',
                templateOptions: {
                  type: 'number',
                  placeholder: '14.34',
                  min: 0,
                  focus,
                  required,
                },
                validators: {
                  twoDecimals: {
                    expression: `!$viewValue.split('.')[1] || $viewValue.split('.')[1].length <= 2`,
                    message: '"Bad amount"'
                  }
                }
              },
              {
                type: 'select',
                className: styles.amountSelect,
                key: 'currencyType',
                defaultValue: 'USD',
                templateOptions: {
                  required,
                  options: [
                    {name: 'USD', value: 'USD'},
                    {name: 'EUR', value: 'EUR'},
                    {name: 'JPY', value: 'JPY'}
                  ]
                },
              }
            ]
          },
        ],
        ...overrides,
      };
    }
  }
};

