import {compileField} from 'test/formly-utils.test';

export default ngModuleName => {
  const compile = compileField.bind(null, ngModuleName);
  describe(`select formly type`, () => {
    it(`should compile without a problem`, () => {
      const {node} = compile({
        type: 'select',
        key: 'foo',
        templateOptions: {
          options: []
        }
      });
      const select = node.querySelector('select');

      expect(
        select.getAttribute('ng-model'),
        'select receives ng-model'
      ).to.equal('model[options.key]');

      expect(
        select.getAttribute('ng-options'),
        'select receives ng-options'
      ).to.exist; // <-- should I check for equals here?

    });

    it(`should allow me to specify my own ngOptions pattern`, () => {
      const {node} = compile({
        type: 'select',
        key: 'foo',
        templateOptions: {
          options: [],
          ngOptions: 'option.name for option in to.options'
        }
      });
      const select = node.querySelector('select');

      expect(
        select.getAttribute('ng-options'),
        'select receives custom ng-options'
      ).to.equal('option.name for option in to.options');
    });
  });
};

