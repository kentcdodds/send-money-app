import createFormlyTypeModule from 'createFormlyTypeModule';
import errorWrapper from '../../wrappers/error';

// and this, right here,
// is one of the reasons
// I don't like angular...
// DSL much?
const defaultNgOptions = [
  `option[to.valueProp || 'value'] `,
  `as option[to.labelProp || 'name'] `,
  `group by option[to.groupProp || 'group'] `,
  `for option in to.options`
].join('');

const ngModuleName = createFormlyTypeModule(addSelect, {
  dependencies: [errorWrapper],
});

export default ngModuleName;

// @ngInject
function addSelect(formlyConfig) {
  // lots of this was stolen
  // from angular-formly-templates-bootstrap
  // ... which I wrote :-)
  formlyConfig.setType({
    name: 'select',
    wrapper: ['error'],
    template: `
      <select
        class="no-arrow"
        ng-model="model[options.key]"
      >
      </select>
      <label
        class="select-arrow"
        ng-bind="to.label"
      >
      </label>
    `,
    defaultOptions(options) {
      /* jshint maxlen:195 */
      const ngOptions = options.templateOptions.ngOptions || defaultNgOptions;
      return {
        ngModelAttrs: {
          [ngOptions]: {value: 'ng-options'}
        }
      };
    },
    apiCheck: check => ({
      templateOptions: {
        options: check.arrayOf(check.object),
        labelProp: check.string.optional,
        valueProp: check.string.optional,
        groupProp: check.string.optional
      }
    })
  });

}

/* istanbul ignore next */
if (process.env.NODE_ENV === 'test') {
  require('./select.test')(ngModuleName);
}

