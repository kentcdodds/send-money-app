import styles from './sm-button-group.css';

import smDirective from 'smDirective';

export default ngModule => {
  ngModule.directive('smButtonGroup', smButtonGroup);

  smButtonGroup.filename = __filename;

  /* istanbul ignore next */
  if (process.env.NODE_ENV === 'test') {
    require('./sm-button-group.test')(ngModule);
  }

  function smButtonGroup() {
    return smDirective({
      restrict: 'E',
      require: 'ngModel',
      template: `
        <div class="${styles.root}" role="group">
          <button
            class="btn ${styles.btn}"
            type="button"
            ng-repeat="option in vm.options"
            ng-bind="option[vm.config.labelKey]"
            ng-click="vm.onButtonClicked(option)"
          >
          </button>
        </div>
      `,
      scope: {
        options: '=',
        config: '=?'
      },
      scopeTypes,
      controllerAs: 'vm',
      bindToController: true,
      controller: SmButtonGroupCtrl,
      link
    });

    // @ngInject
    function SmButtonGroupCtrl() {
      const vm = this;
      vm.config = vm.config || {};
      vm.config.labelKey = vm.config.labelKey || 'label';
      vm.config.valueKey = vm.config.valueKey || 'value';

      vm.onButtonClicked = onButtonClicked;

      function onButtonClicked(option) {
        vm.ngModelCtrl.$setViewValue(option[vm.config.valueKey]);
      }
    }

    function link(scope, el, attrs, ngModelCtrl) {
      scope.vm.ngModelCtrl = ngModelCtrl;
    }

    /* istanbul ignore next */
    function scopeTypes(check) {
      return {
        options: check.arrayOf(check.object),
        config: check.shape({
          labelKey: check.string.optional,
          valueKey: check.string.optional
        }).optional
      };
    }
  }
};

