import {expect} from 'chai';

import _ from 'lodash';
export default ngModule => {
  describe('sm-button-group', () => {
    beforeEach(window.module(ngModule.name));

    let $compile, scope, el, node;
    const basicTemplate = `<sm-button-group ng-model="model" options="options" config="config"></sm-button-group>`;

    beforeEach(inject((_$compile_, $rootScope) => {
      $compile = _$compile_;
      scope = $rootScope.$new();
      scope.model = 'FOO';
      scope.options = [{name: 'Foo', val: 'FOO'}, {name: 'Bar', val: 'BAR'}];
      scope.config = {labelKey: 'name', valueKey: 'val'};
    }));

    it(`should default the label and value keys if not provided`, () => {
      scope.config = {};
      compileAndDigest();
      expect(scope.config).to.containSubset({
        labelKey: 'label',
        valueKey: 'value'
      });
    });

    it(`should update the model value when an option is selected`, () => {
      compileAndDigest();
      const secondButton = node.querySelectorAll('button')[1];
      secondButton.click();
      expect(scope.model).to.equal('BAR');
    });


    function compileAndDigest(template = basicTemplate, extraProps = {}) {
      _.assign(scope, extraProps);
      el = $compile(template)(scope);
      node = el[0];
      scope.$digest();
    }
  });
};

