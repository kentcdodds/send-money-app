import createFormlyTypeModule from 'createFormlyTypeModule';
import errorWrapper from '../../wrappers/error';
import components from './components';

const ngModuleName = createFormlyTypeModule(addButtonGroup, {
  components,
  dependencies: [errorWrapper],
});

export default ngModuleName;

// @ngInject
function addButtonGroup(formlyConfig) {
  formlyConfig.setType({
    name: 'button-group',
    wrapper: ['error'],
    template: `
      <sm-button-group
        ng-model="model[options.key]"
        options="to.options"
        config="to.config"
      >
      </sm-button-group>
    `
  });
}

/* istanbul ignore next */
if (process.env.NODE_ENV === 'test') {
  require('./button-group.test')(ngModuleName);
}

