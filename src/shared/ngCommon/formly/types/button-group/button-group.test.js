import {compileField} from 'test/formly-utils.test';

export default ngModuleName => {
  const compile = compileField.bind(null, ngModuleName);
  describe(`button-group formly type`, () => {
    it(`should compile without a problem`, () => {
      const {node} = compile({
        type: 'button-group',
        key: 'foo',
        templateOptions: {
          options: [],
          config: {}
        }
      });
      const smButtonGroup = node.querySelector('sm-button-group');
      expect(
        smButtonGroup,
        'compiled and is using the sm-button-group'
      ).to.exist;

      expect(
        smButtonGroup.getAttribute('ng-model'),
        'sm-button-group receives ng-model'
      ).to.equal('model[options.key]');

      expect(
        smButtonGroup.getAttribute('options'),
        'sm-button-group receives options'
      ).to.equal('to.options');

      expect(
        smButtonGroup.getAttribute('config'),
        'sm-button-group receives config'
      ).to.equal('to.config');
    });
  });
};

