import styles from './input.css';
import createFormlyTypeModule from 'createFormlyTypeModule';
import errorWrapper from '../../wrappers/error';

const ngModuleName = createFormlyTypeModule(addInput, {
  dependencies: [errorWrapper]
});

export default ngModuleName;

// @ngInject
function addInput(formlyConfig) {
  formlyConfig.setType({
    name: 'input',
    wrapper: ['error'],
    template: `<input ng-model="model[options.key]" class="${styles.input}"/>`
  });
}

/* istanbul ignore next */
if (process.env.NODE_ENV === 'test') {
  require('./input.test')(ngModuleName);
}

