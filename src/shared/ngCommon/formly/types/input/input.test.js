import {compileField} from 'test/formly-utils.test';

export default ngModuleName => {
  const compile = compileField.bind(null, ngModuleName);
  describe(`input formly type`, () => {
    it(`should compile without a problem`, () => {
      const {node} = compile({
        type: 'input',
        key: 'foo'
      });
      const input = node.querySelector('input');

      expect(
        input.getAttribute('ng-model'),
        'input receives ng-model'
      ).to.equal('model[options.key]');
    });
  });
};

