import {compileField} from 'test/formly-utils.test';

export default ngModuleName => {
  const compile = compileField.bind(null, ngModuleName);
  describe(`textarea formly type`, () => {
    it(`should compile without a problem`, () => {
      const {node} = compile({
        type: 'textarea',
        key: 'foo'
      });
      const textarea = node.querySelector('textarea');

      expect(
        textarea.getAttribute('ng-model'),
        'textarea receives ng-model'
      ).to.equal('model[options.key]');
    });
  });
};

