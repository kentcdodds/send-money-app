import createFormlyTypeModule from 'createFormlyTypeModule';
import errorWrapper from '../../wrappers/error';

const ngModuleName = createFormlyTypeModule(addTextarea, {
  dependencies: [errorWrapper],
});

export default ngModuleName;

// @ngInject
function addTextarea(formlyConfig) {
  formlyConfig.setType({
    name: 'textarea',
    wrapper: ['error'],
    template: `
      <textarea
        ng-model="model[options.key]"
      >
      </textarea>
    `
  });
}

/* istanbul ignore next */
if (process.env.NODE_ENV === 'test') {
  require('./textarea.test')(ngModuleName);
}

