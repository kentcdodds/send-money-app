import ngMessages from 'angular-messages';
import createFormlyTypeModule from 'createFormlyTypeModule';

export default createFormlyTypeModule(addError, {
  dependencies: [ngMessages]
});

// @ngInject
function addError(formlyConfig) {
  formlyConfig.setWrapper({
    name: 'error',
    template: `
      <div class="input-wrapper" ng-class="{'error': showError}">
        <formly-transclude></formly-transclude>
        <div
          class="error-overlay +transition-medium"
          ng-messages="fc.$error"
          ng-class="{'+opacity1': showError, '+opacity0': !showError}"
        >
          <div
            ng-message="{{::name}}"
            ng-repeat="(name, message) in ::options.validation.messages"
          >
            {{message(fc.$viewValue, fc.$modelValue, this)}}
          </div>
        </div>
      </div>
    `
  });
}

