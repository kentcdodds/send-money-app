import {bootstrap} from 'test/utils.test';
export default {compileField};

function compileField(ngModuleName, fieldConfig, extraProps = {}, stuffToGet = []) {
  const formHTML = `
    <form name="form">
      <formly-form
        model="model"
        fields="fields"
        form="form"
        options="options"
      >
      </formly-form>
    </form>
  `;

  stuffToGet = [...stuffToGet, '$compile', '$rootScope', '$log'];

  // depend on both formly and the module which will run our function
  const ret = bootstrap(stuffToGet, {ngModuleDeps: ['formly', ngModuleName]});

  const {$compile, $rootScope, $log} = ret;

  // prepare scope for compiling
  const scope = $rootScope.$new();
  Object.assign(scope, {
    model: {},
    fields: [fieldConfig],
    options: {},
    ...extraProps
  });

  // if there is an exception, it's logged
  // use log to make the exception surface
  // in the tests...
  const errors = [];
  $log.error = function errorCatcher() {
    errors.push(arguments);
  };
  const el = $compile(formHTML)(scope);
  scope.$digest();
  if (errors.length) {
    console.error(errors); // eslint-disable-line no-console
    throw new Error('Errors occurred during field compilation, see log!');
  }

  // get the node because I prefer
  // working with real DOM apis
  const node = el[0];

  return {bootstrap: ret, scope, el, node};
}

