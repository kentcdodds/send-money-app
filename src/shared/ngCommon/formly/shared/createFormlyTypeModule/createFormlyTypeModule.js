import angular from 'angular';
import formly from 'angular-formly';
import {arrayify} from 'utils';

export default createFormlyTypeModule;

let moduleNumber = 0;

function createFormlyTypeModule(runFn, options = {}) {
  const deps = options.dependencies || [];
  const ngModule = angular.module(`smFormlyModule${moduleNumber++}`, [...deps, formly]);
  ngModule.run(runFn);
  if (options.components) {
    arrayify(options.components).forEach(component => component(ngModule));
  }
  return ngModule.name;
}


if (process.env.NODE_ENV === 'test') {
  require('./createFormlyTypeModule.test')(createFormlyTypeModule);
}
