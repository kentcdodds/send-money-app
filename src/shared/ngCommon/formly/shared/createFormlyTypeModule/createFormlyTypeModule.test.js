import {noop} from 'lodash';
import angular from 'angular';

export default createFormlyTypeModule => {
  describe(`createFormlyTypeModule`, () => {
    it(`should return an angular module name and put the function on the _runBlocks`, () => {
      const runFn = sinon.spy();
      const ngModuleName = createFormlyTypeModule(runFn);
      expect(ngModuleName).to.be.a('string');
      expect(angular.module(ngModuleName)._runBlocks).to.include(runFn);
    });

    it(`should allow specifying of components`, () => {
      const component = sinon.spy();
      const components = [component];

      const ngModuleName = createFormlyTypeModule(noop, {components});
      expect(component).to.have.been.calledWith(angular.module(ngModuleName));
    });

  });
};
