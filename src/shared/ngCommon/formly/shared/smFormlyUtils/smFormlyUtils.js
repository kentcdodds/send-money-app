const smFormlyUtils = {isInExpressionPropertiesTO, isInTemplateOptions};

const hasTO = optionsHasProperty.bind(null, 'templateOptions');
const hasEP = optionsHasProperty.bind(null, 'expressionProperties');

export default smFormlyUtils;

function isInTemplateOptions(property, options) {
  return hasTO(options) && options.templateOptions.hasOwnProperty(property);
}

function isInExpressionPropertiesTO(property, options) {
  return hasEP(options) && epHas(options.expressionProperties, property);
}

function epHas(ep, property) {
  return (
    ep.hasOwnProperty(`templateOptions.${property}`) ||
    ep.hasOwnProperty(`templateOptions['${property}']`) ||
    ep.hasOwnProperty(`templateOptions["${property}"]`)
  );
}

function optionsHasProperty(property, options) {
  return typeof options === 'object' && options.hasOwnProperty(property);
}

if (process.env.NODE_ENV === 'test') {
  require('./smFormlyUtils.test')(smFormlyUtils);
}

