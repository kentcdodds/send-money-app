export default smFormlyUtils => {
  describe(`smFormlyUtils`, () => {
    describe(`isInTemplateOptions`, () => {
      const fooInTemplateOptions = smFormlyUtils.isInTemplateOptions.bind(null, 'foo');
      it(`should return true if the item exists in expressionProperties`, () => {
        const result = fooInTemplateOptions({templateOptions: {foo: 'bar'}});
        expect(result, 'result').to.be.true;
      });

      it(`should return false if the item does not exist in templateOptions`, () => {
        const result = fooInTemplateOptions({templateOptions: {}});
        expect(result, 'result').to.be.false;
      });

      it(`should return false if there are no templateOptions`, () => {
        const result = fooInTemplateOptions({});
        expect(result, 'result').to.be.false;
      });

      it(`should return false if nothing is passed`, () => {
        const result = fooInTemplateOptions();
        expect(result, 'result').to.be.false;
      });
    });

    describe(`isInExpressionPropertiesTO`, () => {
      const fooInExpressionPropertiesTO = smFormlyUtils.isInExpressionPropertiesTO.bind(null, 'foo');
      it(`should return true if the item exists in expressionProperties`, () => {
        const result = fooInExpressionPropertiesTO({expressionProperties: {'templateOptions.foo': 'bar'}});
        expect(result, 'result').to.be.true;
      });

      it(`should return true if the item exists with a braket selector and single quotes`, () => {
        const result = fooInExpressionPropertiesTO({expressionProperties: {"templateOptions['foo']": 'bar'}});
        expect(result, 'result').to.be.true;
      });

      it(`should return true if the item exists with a braket selector and double quotes`, () => {
        const result = fooInExpressionPropertiesTO({expressionProperties: {'templateOptions["foo"]': 'bar'}});
        expect(result, 'result').to.be.true;
      });

      it(`should return false if the item does not exist in expressionProperties`, () => {
        const result = fooInExpressionPropertiesTO({expressionProperties: {}});
        expect(result, 'result').to.be.false;
      });

      it(`should return false if there are no expressionProperties`, () => {
        const result = fooInExpressionPropertiesTO({});
        expect(result, 'result').to.be.false;
      });

      it(`should return false if nothing is passed`, () => {
        const result = fooInExpressionPropertiesTO();
        expect(result, 'result').to.be.false;
      });
    });
  });
};

