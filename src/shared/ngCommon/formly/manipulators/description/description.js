import styles from './description.css';
import smFormlyUtils from 'smFormlyUtils';
const toHasDescription = smFormlyUtils.isInTemplateOptions.bind(null, 'description');
const epHasDescription = smFormlyUtils.isInExpressionPropertiesTO.bind(null, 'description');

export default descriptionManipulator;

function descriptionManipulator(template, options) {
  const isInTO = toHasDescription(options);
  const isDynamic = epHasDescription(options);
  if (!isInTO && !isDynamic) {
    return template;
  }
  const to = options.templateOptions || {};
  const binding = `${isDynamic ? '' : '::'}to.description`;
  return `
    ${template}
    <div
      for="{{::id}}"
      class="${styles.description} ${to.descriptionClassName || ''}"
      ng-bind="${binding}"
    >
    </div>
  `;
}

if (process.env.NODE_ENV === 'test') {
  require('./description.test')(descriptionManipulator);
}
