import _ from 'lodash';

export default descriptionManipulator => {
  describe(`descriptionManipulator`, () => {
    const basicTemplate = '<input ng-model="model[options.key]" />';
    let defaultOptions, scope, result;

    beforeEach(() => {
      defaultOptions = {};
      scope = {};
    });


    it(`should add the description when templateOptions has a description`, () => {
      manipulate({templateOptions: {description: 'Hello world!'}});
      wasManipulated();
      hasOneTimeBinding();
    });

    it(`should add the description when expressionProperties has reference to a description`, () => {
      manipulate({expressionProperties: {'templateOptions.description': 'options.key'}});
      wasManipulated();
      hasNoOneTimeBinding();
    });

    it(`should not add the description when there is not a reference to description`, () => {
      manipulate();
      wasNotManipulated();
    });


    function wasManipulated() {
      expect(result, 'result has description').to.contain('to.description');
    }

    function wasNotManipulated() {
      expect(result, 'result does not have description').to.not.contain('to.description');
    }


    function hasOneTimeBinding() {
      expect(result, 'result has a one time binding expression').to.contain('::to.description');
    }

    function hasNoOneTimeBinding() {
      expect(result, 'result has no one time binding expression').to.not.contain('::to.description');
    }

    function manipulate(overrideOptions = {}, template = basicTemplate, extraScope) {
      result = descriptionManipulator(
        template,
        {...defaultOptions, ...overrideOptions},
        _.assign(scope, extraScope)
      );
    }
  });

};

