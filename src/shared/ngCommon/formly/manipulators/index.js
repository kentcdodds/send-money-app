export default {
  pre: [],
  post: [
    require('./label'),
    require('./description'),
  ],
};
