import styles from './label.css';
import smFormlyUtils from 'smFormlyUtils';
const toHasLabel = smFormlyUtils.isInTemplateOptions.bind(null, 'label');
const epHasLabel = smFormlyUtils.isInExpressionPropertiesTO.bind(null, 'label');

export default labelManipulator;

function labelManipulator(template, options) {
  const isInTO = toHasLabel(options);
  const isDynamic = epHasLabel(options);
  if (!isInTO && !isDynamic) {
    return template;
  }
  const binding = `${isDynamic ? '' : '::'}to.label`;
  return `
    <label
      for="{{::id}}"
      class="${styles.label}"
      ng-bind="${binding}"
    >
    </label>
    ${template}
  `;
}

if (process.env.NODE_ENV === 'test') {
  require('./label.test.js')(labelManipulator);
}
