import _ from 'lodash';

export default labelManipulator => {
  describe(`labelManipulator`, () => {
    const basicTemplate = '<input ng-model="model[options.key]" />';
    let defaultOptions, scope, result;

    beforeEach(() => {
      defaultOptions = {};
      scope = {};
    });


    it(`should add the label when templateOptions has a label`, () => {
      manipulate({templateOptions: {label: 'Hello world!'}});
      wasManipulated();
      hasOneTimeBinding();
    });

    it(`should add the label when expressionProperties has reference to a label`, () => {
      manipulate({expressionProperties: {'templateOptions.label': 'options.key'}});
      wasManipulated();
      hasNoOneTimeBinding();
    });

    it(`should not add the label when there is not a reference to label`, () => {
      manipulate();
      wasNotManipulated();
    });


    function wasManipulated() {
      expect(result, 'result has label').to.contain('<label');
    }

    function wasNotManipulated() {
      expect(result, 'result does not have label').to.not.contain('<label');
    }


    function hasOneTimeBinding() {
      expect(result, 'result has a one time binding expression').to.contain('::to.label');
    }

    function hasNoOneTimeBinding() {
      expect(result, 'result has no one time binding expression').to.not.contain('::to.label');
    }

    function manipulate(overrideOptions = {}, template = basicTemplate, extraScope) {
      result = labelManipulator(
        template,
        {...defaultOptions, ...overrideOptions},
        _.assign(scope, extraScope)
      );
    }
  });

};

