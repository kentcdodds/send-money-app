import angular from 'angular';
import uiRouter from 'angular-ui-router';

// I wrote these
import azPromiseShow from 'az-promise-show';
import smFormly from './formly';


import services from './services';
import directives from './directives';
import filters from './filters';

const ngModuleName = 'smAppCommon';

export default ngModuleName;
const ngModule = angular
  .module(ngModuleName, [
    uiRouter,
    smFormly,
    azPromiseShow
  ]);

services(ngModule);
directives(ngModule);
filters(ngModule);


