// odd, but it appears that for some reason the global
// `expect` doesn't allow me to use `to.containSubset`
// some odd wizardry no doubt, but I don't have time
// to investigate.
import {expect} from 'chai';
import {bootstrap} from 'test/utils.test';
import {noop} from 'lodash';

export default createState => {
  describe(`createState`, () => {
    it(`should create a state object with defaults`, () => {
      const state = createState({
        url: '/',
        data: {
          foo: 'bar'
        }
      });

      expect(state).to.containSubset({
        url: '/',
        controllerAs: 'vm',
        template: '<div ui-view></div>',
        data: {
          foo: 'bar'
        }
      });

      expect(
        state.data.ngModule,
        'state has ngModule on data'
      ).to.exist;
    });

    it(`should be possible to override the defaults`, () => {
      const state = createState({
        controller: noop,
        controllerAs: null,
        template: '<some-directive></some-directive>'
      });

      expect(state).to.containSubset({
        controller: noop,
        controllerAs: null,
        template: '<some-directive></some-directive>',
      });
    });


    it(`should call the components with the create ngModlue`, () => {
      const components = sinon.spy();
      const state = createState({
        data: {components}
      });
      expect(components).to.have.been.calledWith(state.data.ngModule);
    });

    describe(`controllers`, () => {
      it(`should be created with the given resolves and inject`, () => {
        const {controller} = createState({
          data: {
            inject: ['baz', 'spam']
          },
          resolve: {
            foo: noop,
            bar: noop
          }
        });

        expect(controller).to.have.property('$inject');
        expect(controller.$inject, '4 items to inject').to.have.length(4);

        const injectables = {
          baz: {},
          spam: noop,
          foo: 42,
          bar: false
        };
        const {$controller} = bootstrap('$controller');
        const vm = $controller(controller, injectables);

        Object.keys(injectables).forEach(injectable => {
          expect(controller.$inject, `$inject has ${injectable}`).to.contain(injectable);
          expect(vm[injectable], `vm has been assigned ${injectable}`).to.equal(injectables[injectable]);
        });
      });

      it(`should have an empty $inject if there is no resolve or data.inject`, () => {
        const {controller} = createState({});
        expect(controller.$inject).to.have.length(0);
      });
    });

    describe(`data.run`, () => {
      it(`should add the run function to the ngModule`, () => {
        const run = noop;
        const state = createState({
          data: {run}
        });
        expect(state.data.ngModule._runBlocks).to.include(run);
      });

      it(`should accept an array of run functions`, () => {
        /* eslint func-style:0 */
        const run1 = () => {};
        const run2 = () => {};
        const run = [run1, run2];
        const state = createState({
          data: {run}
        });
        expect(state.data.ngModule._runBlocks).to.include(run1);
        expect(state.data.ngModule._runBlocks).to.include(run2);
      });


    });

  });
};
