import angular from 'angular';
import {arrayify} from 'utils';

/*
 * I built this abstraction for this project specifically
 * No copy/paste going on here :-)
 * I did this to demonstrate that I can develop abstractions
 * which simplify the API for common components (like states
 * in an angular application). It helps the codebase keep
 * a consistent convention (encourages component-based
 * architecture over controllers and templates for example).
 */

export default createState;

function createState(config) {
  const configData = config.data || {};
  const ngModule = createModule(config.name, configData);

  return {
    controller: config.controller ? config.controller : getController(configData, config.resolve),
    controllerAs: 'vm',
    template: '<div ui-view></div>',

    // overrides
    ...config,

    // extras
    data: {
      ...configData,

      // overrides
      ngModule
    }
  };
}

function createModule(name, configData) {
  const ngModuleName = `state${name}Module`;
  const ngModule = angular.module(ngModuleName, configData.dependencies || []);
  if (configData.components) {
    configData.components(ngModule);
  }
  if (configData.run) {
    arrayify(configData.run).forEach(run => ngModule.run(run));
  }
  return ngModule;
}

function getController(data, resolve = {}) {
  const $inject = Object.keys(resolve).concat(data.inject || []);
  GenericStateCtrl.$inject = $inject;
  return GenericStateCtrl;


  function GenericStateCtrl() {
    const vm = this;
    $inject.forEach((injected, index) => {
      vm[injected] = arguments[index];
    });
  }
}

if (process.env.NODE_ENV === 'test') {
  require('./createState.test')(createState);
}
