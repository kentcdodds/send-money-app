/*
 * angular-scope-types is a library that I wrote to bring
 * React's propTypes to angular. It uses api-check which
 * is a VanillaJS implementation of React's propTypes
 * (I wrote api-check as well).
 */

import smApiCheck from 'smApiCheck';
import angularScopeTypesFactory from 'angular-scope-types';

const angularScopeTypes = getAngularScopeTypes();

export default smDirective;

function smDirective(ddo) {
  if (ddo.scopeTypes) {
    return angularScopeTypes.directive(ddo);
  } else {
    return ddo;
  }
}


function getAngularScopeTypes() {
  return angularScopeTypesFactory({
    disabled: process.env.NODE_ENV !== 'development',
    apiCheckInstance: smApiCheck
  });
}
