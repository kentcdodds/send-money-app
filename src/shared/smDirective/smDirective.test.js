import {noop} from 'lodash';

export default smDirective => {
  describe(`smDirective`, () => {
    it(`should accept a directive definition and return that definition with a wrapped controller`, () => {
      const ddo = smDirective({scopeTypes: noop});
      expect(ddo).to.have.property('controller');
    });

    it(`should return the exact directive definition if no scopeTypes is passed`, () => {
      const ddo = smDirective({});
      expect(ddo).to.have.property('controller');
    });
  });

};
