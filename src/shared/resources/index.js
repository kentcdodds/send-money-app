export default {
  images: {
    logo: {
      large: require('./logo-large.png'),
      medium: require('./logo-medium.png')
    }
  }
};
