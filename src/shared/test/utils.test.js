import angular from 'angular';
import _ from 'lodash';
import {arrayify} from 'utils';


export default {bootstrap};

function bootstrap(stuffToGet = [], {ngModuleName, ngModuleDeps = [], innerHTML = ''} = {}) {
  const node = document.createElement('div');
  node.innerHTML = innerHTML;

  if (!ngModuleName) {
    ngModuleName = _.uniqueId('testModule');
    angular.module(ngModuleName, ngModuleDeps);
  }

  mockLog(angular.module(ngModuleName));

  const $injector = angular.bootstrap(node, [ngModuleName]);

  // pass back the injector's version of what's asked for
  const otherObjects = {};
  arrayify(stuffToGet).forEach(item => otherObjects[item] = $injector.get(item));

  // disable $log.info
  const $log = $injector.get('$log');
  const $logInfo = $log.info; // but store it and pass it along in case it's needed
  $log.info = angular.noop;

  return {node, $injector, $logInfo, ...otherObjects};
}

function mockLog(ngModule) {
  ngModule.config($provide => {
    $provide.constant('$log', {
      log: sinon.spy(),
      warn: sinon.spy(),
      error: sinon.spy(),
      info: sinon.spy()
    });
  });
}
