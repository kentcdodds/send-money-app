export default smApiCheck => {
  describe(`smApiCheck`, () => {
    it(`should be disabled when the NODE_ENV is not development`, () => {
      expect(smApiCheck.config.disabled).to.be.true;
    });

    it(`should be an instance of apiCheck`, () => {
      // we could check for additional properties, but this should be enough
      expect(smApiCheck).to.have.property('throw');
    });

    // this is where we'd write tests for any custom checkers

  });
};

