/*
 * api-check is a VanillaJS implementation of
 * React's propTypes which I wrote to help me
 * validate my apis at runtime. It enables
 * angular-scope-types and is used by angular-formly
 * (two other libraries that I wrote for this purpose).
 */

import apiCheck from 'api-check';

const smApiCheck = getSmApiCheck();

export default smApiCheck;

function getSmApiCheck() {
  const disabled = process.env.NODE_ENV !== 'development';
  apiCheck.globalConfig.disabled = disabled;

  return apiCheck({
    output: {prefix: 'send-money:'},
    disabled
  }, {
    // custom checkers...
  });
}

if (process.env.NODE_ENV === 'test') {
  require('./smApiCheck.test')(smApiCheck);
}

