export default {arrayify};

function arrayify(item) {
  if (Array.isArray(item)) {
    return item;
  } else {
    return [item];
  }
}

