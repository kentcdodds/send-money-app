export default utils => {
  describe(`utils`, () => {
    describe(`arrayify`, () => {
      it(`should accept a non-array and convert it to an array`, () => {
        const result = utils.arrayify(true);
        expect(result).to.deep.equal([true]);
      });

      it(`should accept an array and return it`, () => {
        const array = [1, 2, 3];
        const result = utils.arrayify(array);
        expect(result).to.equal(array);
      });
    });
  });
};
