import angular from 'angular';
import bootstrap from './bootstrap';

angular
  .element(document)
  .ready(bootstrap);

