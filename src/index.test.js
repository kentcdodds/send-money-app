import 'angular';
import 'angular-mocks';
import chai from 'chai';
import chaiSubset from 'chai-subset';
import sinonChai from 'sinon-chai';

chai.use(chaiSubset);
chai.use(sinonChai);

window.smConfig = {
  API_BASE_URL: 'https://example.com/api'
};
require('./index');


