#!/usr/bin/env bash
set -e

echo "Linting changed files"
SRC_FILES=$(git diff --staged --diff-filter=ACMTUXB --name-only -- '*.js' | grep -v '\.test\|mock\|e2e\.js$') && x=1
TEST_FILES=$(git diff --staged --diff-filter=ACMTUXB --name-only -- '*.js' | grep '\.test\|mock\|e2e\.js$') && x=1

function lint() {
  if [ "$2" ]; then
    echo "Linting changed $1 files"
    eslint $2 -c other/${1}.eslintrc
  else
    echo "No $1 files changed"
  fi
}

lint "src" $SRC_FILES;
lint "test" $TEST_FILES;

echo "⚡️  changed files passed linting! ⚡️"
