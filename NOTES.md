# Data

I really would have loved to jump on the redux train for this project.
Here are a few reasons I didn't:

1. I've never used redux before. I realize it's API is dead simple, but
   learning something new for a project like this can be dangerous :-)
2. Angular doesn't really work super well with uni-directional dataflow
   :-( Even though that's not really a hard requirement with redux, it's
   a significant benefit that redux enables.

# Repositories (over-architecting much?)

THREE REPOS!?!?!?! Yeah, I know, crazy right. I hope I didn't make the
wrong call here, but I thought it would be valuable for you to see that
I think about the scalability of a codebase. So even though this
codebase doesn't need to scale right now, the projects I expect to work
on at PayPal definitely will need to scale to tens or even hundreds of
thousands of lines of code. This presents a lot of challenges and I
simply wanted to demonstrate that I'm aware of those challenges and know
some good practices to ease the issues with scaling a codebase.

Yes, I did publish the shared module on npm. There are a lot of
different ways to share code between projects. I decided to go about it
this way. I would expect that if PayPal wanted to use npm, we'd do a
private registry (or have an on premises one). npm is a great way to
share code I think :-)

Seriously, I know it looks like I'm over-architecting this solution
quite a bit (because I am). I hope you understand why I did that :-)

